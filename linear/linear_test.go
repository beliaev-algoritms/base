package linear

import "testing"

func TestLinearSearch(t *testing.T) {
	value := 77
	var empty []int
	withoutNumber := []int{11010, 33, 555, -7734, 123123, 54123123, -77, 123123123, 45253}
	withNumberLast := []int{-11010, 33, 555, 7734, 123123, 54123123, -123123123, 77}
	withNumberFirst := []int{77, 33, -555, 7734, 123123, 54123123, 123123123, -123123123}

	if linearSearch(empty, value) {
		t.Fatalf("Value %d must not be found! in %v", value, empty)
	}

	if linearSearch(withoutNumber, value) {
		t.Fatalf("Value %d must not be found! in %v", value, withoutNumber)
	}

	if !linearSearch(withNumberLast, value) {
		t.Fatalf("Value %d must be found! in %v", value, withNumberLast)
	}

	if !linearSearch(withNumberFirst, value) {
		t.Fatalf("Value %d must be found! in %v", value, withNumberFirst)
	}
}
